(function () {

    const buisnessObj = {
        data: {
            legalName: null,
            businessAddress: {
                data: {
                    street: null,
                    city: null,
                    state: null,
                    zip: null
                },
                filled: false
            },
            businessPhone: null,
            ownerSince: null,
            employersNumber: null,
            legalStucture: null,
        },
        filled: false
    };

    function isFilled() {
        const allKeys = Object.keys(buisnessObj.data);
        buisnessObj.filled = true;
        for (let i = 0; i < allKeys.length; ++i) {
            const currentField = buisnessObj.data[allKeys[i]];

            if (allKeys[i] === 'businessAddress') {
                currentField.filled = true;
                const allAddressKeys = Object.keys(currentField.data);
                for (let n = 0; n < allAddressKeys.length; ++n) {
                    if (!isValid(currentField.data[allAddressKeys[n]])) {
                        currentField.filled = false;
                        buisnessObj.filled = false;
                        break;
                    }
                }
            } else {
                if (!isValid(currentField)) {
                    buisnessObj.filled = false;
                }
            }
        }
        const toggle = $('#formToggle');
        if (buisnessObj.filled) {
            toggle.show();
        } else {
            toggle.hide();
        }
    }

    function isValid(val) {
        return val && val.length > 0;
    }

    function handleFieldChange(field, val) {
        if (field.indexOf('businessAddress') > -1) {
            const key = field.split("-")[1];
            buisnessObj.data.businessAddress.data[key] = val;
        } else {
            buisnessObj.data[field] = val;
        }
        isFilled();
    }

    $('.business-field').on('input', function () {
        handleFieldChange(this.id, this.value);
    })

})()

