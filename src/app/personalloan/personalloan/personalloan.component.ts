import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router ,NavigationExtras } from '@angular/router';  
import { TopmenusComponent } from '../../topmenus/topmenus.component';

@Component({
  selector: 'app-businessloan',
  templateUrl: './personalloan.component.html',
  styleUrls: ['./personalloan.component.scss']
})
export class PersonalloanComponent implements OnInit {
  mask: any[] = ['+', '1', ' ', '(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  public input_loan_amount_validation_class : any = "";
  public input_loan_for_validation_class : any = "";

  public loan_ammount : number = null;
  public loan_for : string = "";

  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
  }

  gotoSteps() {
    if(!this.loan_ammount || this.loan_ammount < 5000){
      this.input_loan_amount_validation_class = "invalid-input";
      return false;
    }else{
      this.input_loan_amount_validation_class = "";
    }

    if(!this.loan_for || this.loan_for == ""){
      this.input_loan_for_validation_class = "invalid-input";
      return false;
    }else{
      this.input_loan_for_validation_class = "";
    }

   

    localStorage.setItem('loan_data', JSON.stringify({ loan_ammount: this.loan_ammount, loan_for: this.loan_for }));
    this.router.navigate(['/personalloan/step']);
    
  }

}
