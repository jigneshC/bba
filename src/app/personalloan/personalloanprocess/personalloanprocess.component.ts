import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';  
import {MomentModule} from 'angular2-moment/moment.module';
import * as moment from 'moment';

import { countries } from "./../../datasets";
import { stateList } from "./../../datasets";

import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-businessprocess',
  templateUrl: './personalloanprocess.component.html',
  styleUrls: ['../../../assets/css/steps.css','./personalloanprocess.component.scss']
})
export class PersonalloanprocessComponent implements OnInit {

  mask: any[] = [ '(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];

  countryList: any[] = [];
  stateListA: any[] = [];
  stateListB: any[] = [];

  public currentSteps: any = "about_you";
  public user_home_address: string = "";
  public user_country: string = "";
  public user_state: string = "";
  public user_zip: string = "";

  public business_address: string = "";
  public business_zip: string = "";
  public business_state: string = "";
  public business_country: string = "";

  public zip_placeholderA: string = "zip code";
  public zip_placeholderB: string = "zip code";

  public bus_line_loans: string = "";

  public isStep1Done : boolean = false;
  public isStep2Done : boolean = false;
  public isStep3Done : boolean = false;
  public isStep4Done : boolean = false;

  public isStep1Try : boolean = false;
  public isStep2Try : boolean = false;
  public isStep3Try : boolean = false;
  public isStep4Try : boolean = false;

  public review_12m : number = 0;
  public review_24m : number = 0;
  public review_36m : number = 0;

  public isLoanQualify : boolean = false;

  public yesterDay : any = moment().subtract(1,"day").format('YYYY-MM-DD');

  businessForm : FormGroup;
  userForm : FormGroup;
  financialForm : FormGroup;

  constructor(fb: FormBuilder,private route: ActivatedRoute, private router: Router) { 
    let home_data = JSON.parse(localStorage.getItem("loan_data"));
    if(home_data){
      this.bus_line_loans = home_data.loan_ammount;
    }

    this.businessForm = fb.group({
      'same_address_as_above' : [],
      'business_name' : [null, Validators.required],
      'business_address': [null, Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(50)])],
      'business_country' : [null, Validators.required],
      'business_state' : [null, Validators.required],
      'business_zip' : [null, Validators.required],
      'business_phone' : [null, Validators.required],
      'business_owner_since' : [null, Validators.required],
      'business_total_emp' : [null, Validators.required],
      'business_structure' : [null, Validators.required]
    });

    this.userForm = fb.group({
      'user_name' : [null, Validators.required],
      'user_home_address' : [null, Validators.required],
      'user_country' : [null, Validators.required],
      'user_state' : [null, Validators.required],
      'user_zip' : [null, Validators.required],
      'user_phone' : [null, Validators.required],
      'user_dob' : [null, Validators.required]
    });

    this.financialForm = fb.group({
      'gross_sales' : [null, Validators.required],
      'net_profit' : [null, Validators.required],
      'average_business' : [null, Validators.required],
      'personal_annual_income' : [null, Validators.required],
      'your_owner_ship' : [null, Validators.required],
      'comercial_real_estate_loans' : [null, Validators.required],
      'rent_lease_for_bus_loc' : [null, Validators.required],
      'equipment_vehicle_leases' : [null, Validators.required],
      'bus_line_loans' : [null, Validators.required],
      'is_paing_of' : [],
      'is_opportunity' : []
    });
  }

  gotoHome() {
    this.router.navigate(['/']);
  }
  ngOnInit() {
    this.countryList = countries;
    this.stateListA = stateList;

    this.route.queryParams.subscribe(params => {
      console.log(params);
   });
   
  }

  submitStep1(value: any):void{
    console.log('Reactive Form Data: ')
    console.log(value);
  }

  coppyAddress(value: any){
    if(value){
      this.business_address = this.userForm.value.user_home_address;
      this.business_country = this.userForm.value.user_country;

      this.changeCountry("business_country");
      
      
      this.business_state = this.userForm.value.user_state;
      this.business_zip = this.userForm.value.user_zip;
      
    }
  }

  changeCountry(value: any):void{
    let placeh = "";
    
    if(value == "business_country"){
        this.stateListA = stateList.filter(item => item.country_code == this.business_country);
        this.business_state = "";

        if(this.business_country=="US"){
          placeh = "zip code";
        }else{
          placeh = "postal code";
        }

        this.zip_placeholderA = placeh;
    }else{
       this.stateListB = stateList.filter(item => item.country_code == this.user_country);
       this.user_state = "";
       

        if(this.user_country=="US"){
          placeh = "zip code";
        }else{
          placeh = "postal code";
        }
        this.zip_placeholderB = placeh;
    } 
    
  }

  goToStep(step : any){
    if(step == "about_you"){
      this.isStep1Try = true;
      this.currentSteps = "about_you";
     }else if(step == "about_business"){
      
      if(!this.userForm.valid){
        this.isStep1Try = true;
        this.currentSteps = "about_you";
        return false;
      }else{
          this.isStep2Try = true;
          this.currentSteps = "about_business";
      }
    }else if(step == "financial_info"){
      this.isStep3Try = true;

      if(!this.userForm.valid){
        this.isStep1Try = true;
        this.currentSteps = "about_you";
      }else if(!this.businessForm.valid){
        this.isStep2Try = true;
        this.currentSteps = "about_business";
      }else{
        this.isStep3Try = true;
        this.currentSteps = "financial_info";
      }
    }else if(step == "review_you"){
      this.isStep3Try = true;
      if(!this.userForm.valid){
        this.isStep1Try = true;
        this.currentSteps = "about_you";
      }else if(!this.businessForm.valid){
        this.isStep2Try = true;
        this.currentSteps = "about_business";
      }else if(!this.financialForm.valid){
        this.isStep3Try = true;
        this.currentSteps = "financial_info";
      }else{
        this.isStep3Try = true;
        this.currentSteps = "review_you";
      }
    }
  }

  submitStep(step : any){
    if(step == "about_you"){

      this.isStep1Try = true;
      if(!this.userForm.valid){
        return false;
      }else{
          this.currentSteps = "about_business";
          this.isStep1Done = true;
      }
    }else if(step == "about_business"){
      this.isStep2Try = true;
      if(!this.businessForm.valid){
        return false;
      }else{
          this.currentSteps = "financial_info";
          this.isStep2Done = true;
      }
    }else if(step == "financial_info"){
      this.isStep3Try = true;
      if(!this.financialForm.valid){
        return false;
      }else{
          this.currentSteps = "review_you";
          this.isStep3Done = true;
          this.calcLoan();
      }
    }
    
    
  }

  calcLoan(){
    
    var experience_point = 0;
    var age_point = 0;
    var sales_point = 0;
    var income_point = 0;

    experience_point = (this.businessForm.value.business_owner_since/57.5) * 100;

    var years = moment().diff(this.userForm.value.user_dob, 'years',false);
    if(years<60){
      age_point = (5/57.5) * 100;
    }

    income_point = (this.calcIncomePaymentForPeriodRatio(12,8)/57.5) * 100;

    sales_point = (this.calcSalsePaymentForPeriodRatio(12,8)/57.5) * 100;

    

    var total = (experience_point + age_point + sales_point + income_point);

    console.log(experience_point+ " + " + age_point +" + " +income_point +" + " +sales_point+ " = "+total);
    var score = 0;
    if(total >= 90 ){
      score = 1;
    }else if(total >= 80 && total < 90 ){
      score = 2;
    }else if(total >= 70 && total < 80 ){
      score = 3;
    }else if(total >= 60 && total < 70 ){
      score = 4;
    }else if(total >= 50 && total < 60 ){
      score = 5;
    }else if(total >= 40 && total < 50 ){
      score = 6;
    }else if(total >= 30 && total < 40 ){
      score = 7;
    }else if(total >= 20 && total < 30 ){
      score = 8;
    }else if(total < 20 ){
      score = 9;
    }
    
    if(score==3){
      this.isLoanQualify = true;

      this.review_12m = - this.calcPMT(7.99/(100*12),12,this.bus_line_loans,0,0);

       this.review_24m = - this.calcPMT(9.99/(100*12),24,this.bus_line_loans,0,0);
  
      this.review_36m = - this.calcPMT(10.99/(100*12),36,this.bus_line_loans,0,0);
    }else{
      this.isLoanQualify = false;

      this.review_12m = 0;
      this.review_24m = 0;
      this.review_36m = 0;
      return false;
    }

    

    

  }
  calcIncomePaymentForPeriodRatio(period : number , interest_persiontage:number){

    let TOTAL_PAYMENT_FOR_PERIOD = this.financialForm.value.comercial_real_estate_loans + this.financialForm.value.rent_lease_for_bus_loc +  this.financialForm.value.equipment_vehicle_leases + (interest_persiontage/(100*period)) *  this.financialForm.value.bus_line_loans;
    
    let ratio = this.financialForm.value.net_profit / TOTAL_PAYMENT_FOR_PERIOD;

    
    let res = 0;
    if(ratio < 2){
      res = 0;
    }else if(ratio >= 2 && ratio <3){
      res = 2;
    }else if(ratio >= 3 && ratio <4){
      res = 5;
    }else if(ratio >= 4){
      res = 7.5;
    }
    
    console.log("IncomePayment :"+this.financialForm.value.net_profit+" / "+TOTAL_PAYMENT_FOR_PERIOD + "="+ratio + " : " + res);

    return res;
  }
  calcSalsePaymentForPeriodRatio(period : number , interest_persiontage:number){

    let TOTAL_PAYMENT_FOR_PERIOD = this.financialForm.value.comercial_real_estate_loans + this.financialForm.value.rent_lease_for_bus_loc +  this.financialForm.value.equipment_vehicle_leases + (interest_persiontage/(100*period)) *  this.financialForm.value.bus_line_loans;
    
    let ratio = this.financialForm.value.gross_sales / TOTAL_PAYMENT_FOR_PERIOD;

    
    let res = 0;
    if(ratio < 22){
      res = 0;
    }else if(ratio >= 22 && ratio <25){
      res = 5;
    }else if(ratio >= 25 && ratio <30){
      res = 15;
    }else if(ratio >= 30 && ratio <40){
      res = 25;
    }else if(ratio >= 40 && ratio <50){
      res = 30;
    }else if(ratio >= 50){
      res = 35;
    }
    
    console.log("SalsePayment :"+this.financialForm.value.gross_sales+" / "+TOTAL_PAYMENT_FOR_PERIOD + "="+ratio + " : " + res);

    return res;
  }
  calcPMT(ir, np, pv, fv, type) {
    /*
     * ir   - interest rate per month
     * np   - number of periods (months)
     * pv   - present value
     * fv   - future value
     * type - when the payments are due:
     *        0: end of the period, e.g. end of month (default)
     *        1: beginning of period
     */
    var pmt, pvif;

    fv || (fv = 0);
    type || (type = 0);

    if (ir === 0)
        return -(pv + fv)/np;

    pvif = Math.pow(1 + ir, np);
    pmt = - ir * pv * (pvif + fv) / (pvif - 1);

    if (type === 1)
        pmt /= (1 + ir);

    return pmt;
}


}
