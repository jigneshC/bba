import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { CurrencyMaskModule } from "ng2-currency-mask";
import { TextMaskModule } from 'angular2-text-mask';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { StepsComponent } from './steps/steps.component';
import { TopmenusComponent } from './topmenus/topmenus.component';
import { PersonalloanComponent } from './personalloan/personalloan/personalloan.component';
import { PersonalloanprocessComponent } from './personalloan/personalloanprocess/personalloanprocess.component';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    StepsComponent,
    TopmenusComponent,
    PersonalloanComponent,
    PersonalloanprocessComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    CurrencyMaskModule,
    TextMaskModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
