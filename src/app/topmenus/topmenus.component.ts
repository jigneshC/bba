import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router ,NavigationExtras } from '@angular/router';  

@Component({
  selector: 'app-topmenus',
  templateUrl: './topmenus.component.html',
  styleUrls: ['./topmenus.component.scss']
})
export class TopmenusComponent implements OnInit {

  public ismenuopen : boolean = false;
  public c_route : string = "";
  constructor(private route: ActivatedRoute, private router: Router) {
    this.router.events.subscribe( (event) => {
      if (this.route.routeConfig) {
        this.c_route = this.route.routeConfig.path;
      }
    });
   }

  ngOnInit() {
  }
  togalMenu(){
    if(this.ismenuopen){
      this.ismenuopen = false;
    }else{
      this.ismenuopen = true;
    }
  }
  gotoPage(page:any) {
    this.router.navigate(['/'+page]);
  }
}
