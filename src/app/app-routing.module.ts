import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';     // Add this
import { StepsComponent } from './steps/steps.component'; 
import { PersonalloanComponent } from './personalloan/personalloan/personalloan.component';
import { PersonalloanprocessComponent } from './personalloan/personalloanprocess/personalloanprocess.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'steps',
    component: StepsComponent
  },
  {
    path: 'personalloan',
    component: PersonalloanComponent
  },
  {
    path: 'personalloan/step',
    component: PersonalloanprocessComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
